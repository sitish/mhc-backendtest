**Tech Stack**

Tech stack that i used:

- Engine:Node Js
- Framework:Express js
- Database: Mongodb (using mongoose)
- Authentication & Authorization: JWT, Passport
- Unit Testing: Jest


## Install & Usage

Start by cloning this repository

```sh
# HTTPS
$ git clone https://sitish@bitbucket.org/sitish/mhc-backendtest.git
```

then

```sh
# cd into project root
$ npm i
# test the code
$ npm test
# run the api in development mode
$ npm  run dev

```
## Documentation

documentation created by Postman.
url documentation: https://documenter.getpostman.com/view/7578520/TzK17FX2

