require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

// Express
const express = require("express");

// Import routes
const authRoutes = require("./routes/authRoutes");
const coinMarketCapRoutes = require("./routes/coinMarketCapRoutes");

// Make express app
const app = express();

// Body-parser to read req.body
app.use(express.json()); // Enable req.body JSON type
app.use(
  express.urlencoded({
    extended: true,
  })
); // Support urlencode body

// Set static file directory
app.use(express.static("public"));

// Make routes
app.get("/", (req, res) => {
  return res.redirect(
    "https://documenter.getpostman.com/view/7578520/TzK17FX2"
  );
});
app.use("/auth", authRoutes);
app.use("/cryptocurrency", coinMarketCapRoutes);

if (process.env.NODE_ENV !== "test") {
  // Running server
  app.listen(3000, () => console.log("Server running on 3000"));
}

module.exports = app;
