const request = require("supertest"); // Import supertest
const app = require("../index"); // Import app
const { user } = require("../models"); // Import user and transaksi models
var token;
// Delete all data in user and transaksi
beforeAll(async () => {
  await user.deleteMany();
});

// Test the auth
describe("Auth Test", () => {
  describe("/auth/signup POST", () => {
    it("It should create a user and return the token", async () => {
      const res = await request(app).post("/auth/signup").send({
        email: "sitish272@gmail.com",
        password: "Aneh1234!!",
        confirmPassword: "Aneh1234!!",
        name: "Siti H",
      });

      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Success");
      expect(res.body).toHaveProperty("token");
      expect(res.body).toHaveProperty("data");
    });

    it("It should error when create a user because email already in used", async () => {
      const res = await request(app).post("/auth/signup").send({
        email: "sitish272@gmail.com",
        password: "Aneh1234!!",
        confirmPassword: "Aneh1234!!",
        name: "Siti H",
      });

      expect(res.statusCode).toEqual(400);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Email already registered");
    });

    it("It should error because email already registered and incorrect password combination", async () => {
      const res = await request(app).post("/auth/signup").send({
        email: "sitish272@gmail.com",
        password: "password",
        confirmPassword: "password",
        name: "Siti H",
      });

      expect(res.statusCode).toEqual(400);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Email already registered, Password needs (uppercase & lowercase characters, number, and symbol)");
    });

    it("It should error because password and confirmPassword must be same", async () => {
      const res = await request(app).post("/auth/signup").send({
        email: "test@gmail.com",
        password: "Password!123",
        confirmPassword: "Password!1234",
        name: "test",
      });

      expect(res.statusCode).toEqual(400);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Password confirmation must be same as password");
    });

  

  
  });

  describe("/auth/signin POST", () => {
    it("It should return the token", async () => {
      const res = await request(app).post("/auth/signin").send({
        email: "sitish272@gmail.com",
        password: "Aneh1234!!",
      });

      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Success");
      expect(res.body).toHaveProperty("token");
      expect(res.body.data).toBeInstanceOf(Object);
      token=res.body.token;
    });

    it("It should return error because email not registered", async () => {
      const res = await request(app).post("/auth/signin").send({
        email: "email@gmail.com",
        password: "Aneh1234!!",
      });

      expect(res.statusCode).toEqual(401);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Email not found");
    });
  });

  describe("/cryptocurrency/map GET", () => {
    it("It should return list of currency", async () => {
      const res = await request(app).get("/cryptocurrency/map").set("Authorization", "Bearer " + token);

      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Success");
      expect(res.body).toHaveProperty("data");
      expect(res.body.data).toBeInstanceOf(Object);
    });
  });
});
