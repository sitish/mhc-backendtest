const rp = require('request-promise'); //import request promise

function getData(obj) {
  //collect in variable data
  const data={
    method: 'GET',
    uri: 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/'+obj.key,
    qs: {...obj.params},
    headers: {
      'X-CMC_PRO_API_KEY': process.env.API_KEY
    },
    json: true,
    gzip: true};
  return data;
}


class CoinMarketCapController {
 
  //CoinMarketCap ID map
  async map(req, res) {
    const {
      start, listing_status , status, limit, sort, symbol,
    } = req.query;
    try {

      //set params and key
      let obj={key:"map"};
      obj.params={};
      if(start) obj.params.start=start;
      if(listing_status) obj.params.listing_status=listing_status;
      if(limit) obj.params.limit=limit;
      if(sort) obj.params.sort=sort;
      if(symbol) obj.params.symbol=symbol;

      //get data
      const data= await rp(getData(obj));

      // if success
      return res.status(200).json({
        message: "Success",
        page:start,
        limit:limit,
        data:data.data,
      });
    } catch (e) {
      // if error
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  };

  // async info(req, res) {
  //   try {
  //     const result= await rp(getData("info"));

  //     return res.status(200).json({
  //       message: "Success",
  //       data:result,
  //     });
  //   } catch (e) {
  //     return res.status(500).json({
  //       message: "Internal Server Error",
  //       error: e.message,
  //     });
  //   }
  // };

//   async listing_latest(req, res) {
//     try {
//       const result= await rp(getData("listing/latest"));

//       return res.status(200).json({
//         message: "Success",
//         data:result,
//       });
//     } catch (e) {
//       return res.status(500).json({
//         message: "Internal Server Error",
//         error: e.message,
//       });
//     }
//   };

//   async listing_historical(req, res) {
//     try {
//       const result= await rp(getData("listing/historical"));

//       return res.status(200).json({
//         message: "Success",
//         data:result,
//       });
//     } catch (e) {
//       return res.status(500).json({
//         message: "Internal Server Error",
//         error: e.message,
//       });
//     }
//   };

//   async quotes_latest(req, res) {
//     try {
//       const result= await rp(getData("quotes/latest"));

//       return res.status(200).json({
//         message: "Success",
//         data:result,
//       });
//     } catch (e) {
//       return res.status(500).json({
//         message: "Internal Server Error",
//         error: e.message,
//       });
//     }
//   }
}

module.exports = new CoinMarketCapController();