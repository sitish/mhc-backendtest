const express = require("express"); // Import express

// Import controller
const coinMarketCapController = require("../controllers/coinMarketCapController");

// Import auth (middleware)
const auth = require("../middlewares/auth");

// Make router
const router = express.Router();

// coinMarketCap
router.get("/map",auth.user,coinMarketCapController.map);
// router.get("/info",coinMarketCapController.info);
// router.get("/listing/latest",coinMarketCapController.listing_latest);
// router.get("/listing/historical",coinMarketCapController.listing_historical);
// router.get("/quotes/latest",coinMarketCapController.quotes_latest);
// router.get("/quotes/historical",coinMarketCapController.quotes_historical);
// router.get("/market-pairs/latest",coinMarketCapController.market_pairs_lates);
// router.get("/ohlcv/latest",coinMarketCapController.ohlcv_latest);
// router.get("/ohlcv/historical",coinMarketCapController.ohlcv_historical);
// router.get("/price-performance-stats/latest",coinMarketCapController.price_performance_stats_latest);


module.exports = router; // Export router
